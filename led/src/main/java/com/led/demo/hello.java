package com.led.demo;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
@RestController
@Controller
public class hello {
    String light_status = "";


    @RequestMapping("/index/ctrl")
    public Map<String,Object> hello_1(@RequestBody String led){
        light_status = led;
        System.out.println("小程序指令:"+light_status);
        Map<String,Object> map = new HashMap<>();
        String str = "";
        if(led.contains("on"))
        {
            str = "on";
        }
        else if(led.contains("off"))
        {
            str = "off";
        }
        map.put("status",str);
        return map;
    }


    @RequestMapping("/index/led") //, HttpServletResponse response
    public  List<Map<String,Object>> receive_led (@RequestBody Map<String, Object> resmap){
        String type = resmap.get("status").toString();
        System.out.println("树莓派当前状态:"+type);
        Map<String,Object> r = new HashMap<>();
        if(type.contains("off") && light_status.contains("on"))
        {
            r.put("status","on");
        }
        else if(type.contains("on") && light_status.contains("off"))
        {
            r.put("status","off");
        }
        else
        {
            r.put("status","0");
        }
        List<Map<String,Object>> res = new ArrayList<>();
        res.add(r);
        //.getWriter().write("on");
        return res;
    }

}
